
from time import sleep
import os, pygame
from pygame.locals import *
from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *
import numpy

if __name__ == "__main__":
    main_dir = os.getcwd()
else:
    main_dir = os.path.split(os.path.abspath(__file__))[0]

data_dir = os.path.join(main_dir, 'data')

def handleInput(screen):

    for event in pygame.event.get():
        if event.type == QUIT:
            return True
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                return True
            elif event.key == K_s:
                pygame.event.set_blocked(KEYDOWN|KEYUP)
                fname = raw_input("File name? ")
                pygame.event.set_blocked(0)
                pygame.image.save(screen,fname)
    return False

def ringWorld(nloops=2, nspheres=100, nspirals=3):
    myLights = [Light((1,1,1))]
    myObjects = []
    floops = float(nloops) + 1.0/nspirals
    radius1 = 500
    radius2 = 100
    for i in range(nspheres):
        angle1 = 2*numpy.pi*nspirals*i/float(nspheres)
        angle2 = floops*angle1
        s1 = numpy.sin(angle1)
        s2 = numpy.sin(angle2)
        c1 = numpy.cos(angle1)
        c2 = numpy.cos(angle2)
        w = radius1 + c2*radius2
        h = s2*radius2
        x = c1*w
        y = s1*w
        z = h
        pct = 6.0*float(i)/float(nspheres)
        if 0 <= pct and pct < 1:
            color = (1,pct,0)
        elif 1 <= pct and pct < 2:
            pct = pct - 1
            color = (1-pct, 1, 0)
        elif 2 <= pct and pct < 3:
            pct = pct - 2
            color = (0,1,pct)
        elif 3 <= pct and pct < 4:
            pct = pct - 3
            color = (0,1-pct,1)
        elif 4 <= pct and pct < 5:
            pct = pct - 4
            color = (pct,0,1)
        else:
            pct = pct - 5
            color = (1,0,1-pct)
        myObjects.append(Sphere((x, y, z - 2000),
                                60,
                                Phong(color)))
    myEye = vec(0,0,150)
    myCamera = Camera(eye = myEye,
                      ul = (-50,50,0)-myEye,
                      ur = (50,50,0)-myEye,
                      ll = (-50,-50,0)-myEye,
                      lr = (50,-50,0)-myEye
                      )
    return World(myObjects, myLights, myCamera)


def main():
    pygame.init()
    screen = pygame.display.set_mode((512,512))
    pygame.display.set_caption('Raytracing!')
    
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((1,0,0))
    
    screen.blit(background, (0, 0))
    pygame.display.flip()
    
    going = True
    pixelsize = 128 # power of 2
    width, height = screen.get_size()

    #world = ThreeSpheres()
    
    #world = RandomSpheres(n=32)
    #world = World()
    myLights = [Light(direction=(1,1,1),color=(1,1,1))]
    world = World([
                   Sphere((-3,-2,6),1,Phong(color=(1,1,0))),
                   Sphere((3,-3,-4),2,Phong(color=(1,0.5,0.25))),
                   Sphere((-4,-1,-.5),2,Reflector()),
                   Plane((0,-1,0), (0,10,0), Flat(color = (.5,.5,.5))),
                         
                   Plane((0,1,0), (0,-3,0), Flat(color = (.25, .25, .25)))],
                   myLights,
                   Camera())
    # world = ringWorld()

    while going:
        going = not(handleInput(screen))
        while pixelsize > 0:
            for x in range(0,width,pixelsize):
                xx = x/float(width)
                for y in range(0,height,pixelsize):
                    #clock.tick(2)
                    yy = y/float(height)
                    # draw into background surface
                    color = world.colorAt(xx,yy)
                    color = [int(255*c) for c in color]
                    r,g,b = color
                    color = pygame.Color(r,g,b,255)#.correct_gamma(1/2.2)
                    background.fill(color, ((x,y),(pixelsize,pixelsize)))

            if handleInput(screen):
                return
                    
            #draw background into screen
            screen.blit(background, (0,0))
            pygame.display.flip()
            
            print(pixelsize)
            pixelsize /= 2

#if this file is executed, not imported
if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()

