from material import *
from ray import *
from vectors import *


class AbstractWorld():
    def colorAt(self, x, y):
        pass
    
class World(AbstractWorld):
    def __init__(self, 
                 objects=None, 
                 lights=None,
                 camera=None,
                 maxDepth = 3,
                 neutral = vec(.25,.5,1),
                 fogBegin = 1.0e10,
                 fogEnd = 1.0e10,
                 nsamples = 1):
        if not(objects):
            objects = [Sphere()]
        if not(lights):
            lights = [Light()]
        if not(camera):
            camera = Camera()

        self.objects = objects
        self.lights = lights
        self.camera = camera
        self.maxDepth = maxDepth
        self.neutral = vec(neutral)
        self.nsamples = nsamples
        self.fogBegin = fogBegin
        self.fogEnd = fogEnd


    def colorAt(self, x, y):
        ray = self.camera.ray(x,y)
        if ray.depth > self.maxDepth:
            return None
        dist, point, normal, obj = ray.closestHit(self)
        if obj != None:
            if dist > self.fogEnd:
                return self.neutral
            elif dist < self.fogBegin:
                color = obj.material.colorAt(point,
                                             normal,
                                             ray,
                                             self)
                if color == None:
                    print "none color"
                    return self.neutral
                else:
                    return clamp(color, 0, 1)
            else:
                fogPortion = (dist-self.fogBegin)/(self.fogEnd-self.fogBegin)     
                color = obj.material.colorAt(point,
                                             normal,
                                             ray,
                                             self)
                if color == None:
                    print "none color"
                    return self.neutral
                finalColor = (1-fogPortion)*color + (fogPortion)*self.neutral
                return clamp(finalColor, 0, 1)
        else:
            return self.neutral
        


        
        
