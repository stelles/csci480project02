from numpy import *



def vec(x,y=None,z=None):
	"""return a numpy.array with type numpy.float"""
	if y == None and z == None:
		return float32(array([x[0],x[1],x[2]]))
	else:
		return float32(array([x,y,z]))
	
def normalize(v):
	"""return a normalized vector in direction v"""
	length = sqrt((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]))
	if length == 0:
		return vec(1,0,0)
	return vec(v[0]/length, v[1]/length, v[2]/length)


def reflect(v, normal):
	"""return a v reflected through normal"""
	return 2*dot(v, normal)*normal - v

def posDot(v, n):
	return max(0.0, dot(v,n))

def clamp(v, lo, hi):
	v = [max(lo, x) for x in v]
	v = [min(hi, x) for x in v]
	return vec(v)

def __sub__(self, v):
	return vec(self[0]-v[0], self[1]-v[1], self[2]-v[2])

