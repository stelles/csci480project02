from vectors import *
from ray import *
import numpy as N
MAX_DEPTH = 3

class Material():
    def colorAt(self, point, normal, ray, world):
        return vec(1,1,1)

class Flat(Material):
    def __init__(self, color = (0.25, 0.5, 1.0)):
        self.color = color

    def colorAt(self, point, normal, ray, world):
        return self.color


class Phong(Material):
    def __init__(self,
                color=(0.25, 0.5, 1.0),
                specularColor=(1,1,1),
                ambient = 0.2,
                diffuse = 1.0,
                specular = 0.5,
                shiny = 64):
        self.color = vec(color[0], color[1], color[2])
        self.specularColor = vec(specularColor[0], specularColor[1], specularColor[2])
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny


    def phongAt(self,
                color,
                specularColor,
                point,
                normal,
                ray,
                world):
        #from ray import *

        lights = world.lights
        eyeVector = -ray.vector
        c = self.ambient*color

        if N.dot(normal,eyeVector) <= 0:
            normal = vec([-x for x in normal])
        for light in lights:

            lightVector = normalize(light.direction)
            point = point + normal


            ambientAndDiffuse = color*light.color
            spec = specularColor*light.color
            
            reflectedLight = reflect(lightVector, normal)
            

            lightRay = Ray(point, lightVector)

            if not lightRay.anyHit(world, light):
                c += self.diffuse * posDot(normal, lightVector) * ambientAndDiffuse
                c += self.specular * posDot(eyeVector, reflectedLight)**self.shiny * spec
        return clamp(c, 0.0, 1.0)

    def colorAt(self,
            point,
            normal,
            ray,
            world):
        return self.phongAt(self.color,
                        self.specularColor,
                        point,
                        normal,
                        ray,
                        world)


class Reflector(Phong):
    def __init__(self,
                 color=(0.25, 0.5, 1.0),
                 specularColor=(1,1,1),
                 ambient = 0.2,
                 diffuse = 1.0,
                 specular = 0.5,
                 shiny = 64):
        self.color = vec(color)
        self.specularColor = vec(specularColor)
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny

    def colorAt(self,
                point,
                normal,
                ray,
                world):
        from ray import *
        


        lights = world.lights
        eyeVector = -ray.vector

        if N.dot(normal,eyeVector) < 0:
            normal = -normal
        for light in lights:

            lightVector = normalize(light.direction)
            point = point + normal
            

            
            
            reflectedLight = reflect(lightVector, normal)

            if ray.depth > MAX_DEPTH:
                return self.color

            reflectedRay = Ray(point, normalize(reflectedLight), ray.depth+1)    
            obj = reflectedRay.closestHit(world)
            if obj[3] == None:
                color = self.color
            else:
                color = obj[3].material.colorAt(obj[1], obj[2], reflectedRay, world)


            
            c = [self.ambient*x for x in color]
            ambientAndDiffuse = color*light.color
            spec = self.specular*light.color
        
            c += self.diffuse * posDot(normal, lightVector) * ambientAndDiffuse
            c += self.specular * posDot(eyeVector, reflectedLight)**self.shiny * spec
        return clamp(c, 0.0, 1.0)