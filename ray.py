from vectors import *
from numpy import *

class AbstractRay():
    def pointAt(self, distance):
        """return the point at distance along the ray"""
        return (0,0,0)
    def closestHit(self, scene):
        """return (dist,point,normal,obj)"""
        return (None, None, None, None)
    def anyHit(self, scene, light):
        return False

        
class Ray(AbstractRay):
    def __init__(self, point, vector, depth=0):
        self.point = point
        self.vector = normalize(vector)
        self.depth = depth

    def pointAt(self, distance):
        """return the point at distance along the ray"""
        return self.point + self.vector*distance    

    def closestHit(self, world):
        """return (dist,point,normal,obj)"""    
        
        dist, point, norm, obj = None, None, None, None  
        for o in world.objects:
            hit = o.hit(self)
            if hit[0] != None:
                if dist == None:
                    dist,point,norm,obj = hit
                elif hit[0] < dist:
                    dist,point,norm,obj = hit
        return (dist,point,norm,obj)

    def anyHit(self, scene, light):
        for obj in scene.objects:
            if obj.castShadows():
                dist,point,n,o = obj.hit(self)
                if dist != None:
                    return True
        return False