EPSILON = 1.0e-10
import numpy as N
from vectors import *
from material import *


class GeometricObject():
    def hit(self, ray):
        """Returns (t, point, normal, object) if hit
        and t > EPSILON"""
        return (None, None, None, None)
        
class Sphere(GeometricObject):
    def __init__(self, point=(0,0,0), radius=1,
                            material=None, shadow=True):
        self.point = vec(point[0], point[1], point[2])
        self.radius = radius
        self.shadow = shadow
                            
        if not(material):
            material = Phong()
        self.material = material

    def hit(self, ray):
        # assume sphere at origin, so translate ray:
        raypoint = ray.point - self.point
        a = N.dot(ray.vector, ray.vector)
        b = 2*N.dot(raypoint, ray.vector)
        c = N.dot(raypoint, raypoint) - self.radius*self.radius
        disc = b*b - 4*a*c
        if disc > 0.0:
            t = (-b-N.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)
            t = (-b+N.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)
        return (None, None, None, None)

    def normalAt(self, point):
        return normalize(point - self.point)

    def castShadows(self):
        return self.shadow

class Shape():
    def hit(self, ray):
        return (None, None, None, None)

class Plane(Shape):
    def __init__(self, point, normal, material, shadow=False):
        self.point = point
        self.normal = normal
        self.material = material
        self.shadow = shadow

    def castShadows(self):
        return self.shadow

        
    def hit(self, ray):
        p = ray.point
        v = normalize(ray.vector)
        n = normalize(self.normal)
        q = self.point
        """Returns (t, point, normal, object) if hit and t > EPSILON"""

        if N.dot(v, n) <= 0.0:
            return (None, None, None, None)
        else:
            t = N.dot(n, (q - p))/N.dot(n, v)
            print t
            point = p + t*v
            print point

            return (-t, point, self.normal, self)

