from vectors import *
from ray import *
from numpy import *

class AbstractCamera():
    def ray(self, x, y):
        return Ray((0,0,0),(0,0,-1))

class Camera(AbstractCamera):
    def __init__(self,
                eye = (0,0,10),
                ul = (-10,10,-10),
                ur = (10,10,-10),
                ll = (-10,-10,-10),
                lr = (10, -10, -10)):
        self.eye = vec(eye[0],eye[1],eye[2])
        self.ul = vec(ul[0],ul[1],ul[2])
        self.ur = vec(ur[0],ur[1],ur[2])
        self.ll = vec(ll[0],ll[1],ll[2])
        self.lr = vec(lr[0],lr[1],lr[2])


    def ray(self, x, y):
        x1 = self.ul*(1-x) + self.ur*x
        x2 = self.ll*(1-x) + self.lr*x
        p = x1 + (x2-x1)*y
        return Ray(self.eye, normalize(p))